class Event < ApplicationRecord
	belongs_to :program
	has_many :event_users
	accepts_nested_attributes_for :event_users, allow_destroy: true

	def end_time
		Event.where("from > ?", Time.now)
	end
end
