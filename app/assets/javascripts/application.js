// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require rails-ujs
//= require turbolinks
//= require just_datetime_picker/nested_form_workaround
//= legacy
//= picker.date
//= picker.time
//= require_tree .


$(document).ready(function () {
	$('#grid_view').click(function (e){
		$('.grid').css('display','block');
		$('table').css('display','none');
		e.preventDefault();
	});
	$('#list_view').click(function (e){
		$('table').css('display','block');
		$('.grid').css('display','none');
		e.preventDefault();
	});
});
