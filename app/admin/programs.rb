ActiveAdmin.register Program do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :description, :start_date, :end_date, events_attributes: [:id, :name, :program_id, :to, :from, :_destroy]
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

form do |f|
	f.input :name
	f.input :description
	f.input :start_date, as: :datepicker
	f.input :end_date, as: :datepicker

	f.inputs "Events" do
		f.has_many :events, allow_destroy: true do |a|
			a.input :name
			a.input :to, :as => :time_picker
			a.input :from, :as => :time_picker
		end 
	end
	f.actions
end



end
