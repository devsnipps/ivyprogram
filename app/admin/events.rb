ActiveAdmin.register Event do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :to, :from, event_users_attributes: [:id, :name, :event_id, :email, :_destroy]
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

form do |f|
	f.input :name
	
	f.input :to#, :as => :date_picker
	f.input :from#, :as => :date_picker

	f.inputs "Event Users" do
		f.has_many :event_users, allow_destroy: true do |a|
			a.input :name
			a.input :email
			
		end 
	end
	f.actions
end

end
