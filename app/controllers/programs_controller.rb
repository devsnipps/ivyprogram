class ProgramsController < ApplicationController
  def index
  	
  	@p = Program.search(params[:q])
  	@programs = @p.result
  end
  def show
  	@program = Program.find(params[:id])
  end
end
