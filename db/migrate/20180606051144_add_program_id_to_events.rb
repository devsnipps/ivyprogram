class AddProgramIdToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :program_id, :integer
  end
end
